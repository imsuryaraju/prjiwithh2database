package com.example.PrjtWithH2Database.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.PrjtWithH2Database.Entity.ListOfBooks_EntityClass;
import com.example.PrjtWithH2Database.Repository.ListOfBooks_RepoClass;

@RestController
public class ListOfBooks_ControllerClass {
    
	@Autowired
	private ListOfBooks_RepoClass RepoClassRef;
	
	@PostMapping("/ListOfBook")
	public HttpStatus save(@RequestBody ListOfBooks_EntityClass listOfBooks_EntityClass)
	{
		RepoClassRef.save(listOfBooks_EntityClass);
		return HttpStatus.CREATED;
	      	
	}
	@PostMapping("/ListOfBooks")
	public List<ListOfBooks_EntityClass> saveAll(@RequestBody List<ListOfBooks_EntityClass> listOfBooks)
	{
		return RepoClassRef.saveAll(listOfBooks);
	        	
	}
	@GetMapping("/get")
	public List<ListOfBooks_EntityClass> getMethod()
	{
		return RepoClassRef.findAll();
	}
	@GetMapping("/get/{id}")
	public Optional<ListOfBooks_EntityClass> getById(@PathVariable Long id)
	{
		return RepoClassRef.findById(id);
	}
	@DeleteMapping("/delete/{id}")
	public String deleting(@PathVariable Long id)
	{
		RepoClassRef.deleteById(id);
		return "Id " +id+ " has been deleted";
	}

}
