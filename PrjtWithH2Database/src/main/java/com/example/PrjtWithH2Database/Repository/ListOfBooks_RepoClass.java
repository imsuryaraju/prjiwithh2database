package com.example.PrjtWithH2Database.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.PrjtWithH2Database.Entity.ListOfBooks_EntityClass;

@Repository
public interface ListOfBooks_RepoClass extends JpaRepository<ListOfBooks_EntityClass, Long> {

	

}
